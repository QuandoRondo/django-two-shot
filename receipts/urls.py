from django.urls import path
from .views import receipt_list_view


app_name = 'receipts'


urlpatterns = [
    path('', receipt_list_view, name='home'),
]
