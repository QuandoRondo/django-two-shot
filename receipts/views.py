from django.shortcuts import render
from .models import Receipt



# Create your views here.

def receipt_list_view(request):
    receipts = Receipt.objects.all()
    context = {'receipts': receipts}
    return render(request, 'receipts/receipt_list.html', context)
